#!/usr/bin/python
#
# Copyright (c) 2021, Texas Instruments Incorporated
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# *  Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# *  Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# *  Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

from __future__ import print_function
import re
import sys

if len(sys.argv) != 2:
    print()  # Empty line
    print(sys.argv[0], "help:\n")
    print("Please pass the rd1 file to this script, e.g.:")
    print("", sys.argv[0], "am57xx-avs-abb_yyyy-mm-dd_hhmmss.rd1\n")
    print("Output file will have same base name as input, "
          "but csv file type.\n")
    sys.exit(1)

# Inputs:
#   Data - 32-bit register value
#   Upper - Highest bit to keep
#   Lower - Lowest bit to keep
#   (bit 0 refers to LSB, bit 31 to MSB)
# Return: right aligned data
def bits32(data, upper, lower):
    data = data >> lower # unsigned right-shift
    upper = upper - lower
    bitmask =  0xFFFFFFFF >> (31 - upper)
    return (data & bitmask)

def get_data(reg_addr):
    valid = 1
    try:
        reg_index = address_list.index(reg_addr)
    except ValueError:
        print("Error finding address", hex(reg_addr))
        valid = 0
        return (valid, 0)
    return (valid, data_list[reg_index])

# Input: address of CTRL_CORE_STD_FUSE_OPP_VMIN_x register
# Returns decoded values for voltage, abb_en, and the ABB LDO voltage
def decode_fuse_opp_vmin_reg(reg_addr):
    valid = 1
    value = get_data(reg_addr)
    if value[0]:
        voltage = bits32(value[1], 11, 0)
        abb_en = bits32(value[1], 25, 25)
        vsetabb = bits32(value[1], 24, 20)
        return (valid, voltage, abb_en, vsetabb)
    else:
        return (0, 0, 0, 0)

def decode_ldovbb_voltage_ctrl(reg_addr):
    valid = 1
    value = get_data(reg_addr)
    if value[0]:
        fbb_mux_ctrl = bits32(value[1], 10, 10)
        fbb_vset_in = bits32(value[1], 9, 5)
        fbb_vset_out = bits32(value[1], 4, 0)
    else:
        valid = 0
        fbb_mux_ctrl = 0
        fbb_vset_in = 0
        fbb_vset_out = 0
    return (valid, fbb_mux_ctrl, fbb_vset_in, fbb_vset_out)

try:
    rd1 = open(sys.argv[1], "rt")
except IOError:
    print("Error: input file", sys.argv[1], "not accessible.")
    sys.exit(1)

try:
    csv_filename = sys.argv[1]
    csv_filename = csv_filename.replace(".rd1", ".csv")
    csv = open(csv_filename, "w+")
except IOError:
    print("Error creating file", csv_filename)
    sys.exit(1)

# Throw away header row
header = rd1.readline()

# Read rd1 file into separate lists for address and data
address_list = [] # empty list to contain addresses
data_list = [] # empty list to contain data
for lines in rd1:
    # Use regular expression to extract address and data from rd1
    m = re.match(r'(0x[0-9a-fA-F]{8})\s+(0x[0-9a-fA-F]{8})', lines, 0)
    if m:
        alphanum1 = m.group(1)  # address (string)
        alphanum2 = m.group(2)  # data (string)
        address = int(alphanum1, 16)  # convert from string to number
        register_value = int(alphanum2, 16)  # convert from string
        address_list.append(address)
        data_list.append(register_value)

csv_string = "%s,,,,0x%08x,\r\n"

# CTRL_WKUP_ID_CODE
value = get_data(0x4AE0C204)
if value[0]:
    csv.write(csv_string % ("CTRL_WKUP_ID_CODE",value[1]))
    if value[1] == 0x0BB5002F:
        device_type = 574

# CTRL_WKUP_STD_FUSE_DIE_ID_0
value = get_data(0x4AE0C200)
if value[0]:
    csv.write(csv_string % ("CTRL_WKUP_STD_FUSE_DIE_ID_0",value[1]))

# CTRL_WKUP_STD_FUSE_DIE_ID_1
value = get_data(0x4AE0C208)
if value[0]:
    csv.write(csv_string % ("CTRL_WKUP_STD_FUSE_DIE_ID_1",value[1]))

# CTRL_WKUP_STD_FUSE_DIE_ID_2
value = get_data(0x4AE0C20C)
if value[0]:
    csv.write(csv_string % ("CTRL_WKUP_STD_FUSE_DIE_ID_2",value[1]))

# CTRL_WKUP_STD_FUSE_DIE_ID_3
value = get_data(0x4AE0C210)
if value[0]:
    csv.write(csv_string % ("CTRL_WKUP_STD_FUSE_DIE_ID_3",value[1]))

csv.write("\r\n")

# CSV files must use \r\n for all line endings
# Create header row
csv.write("Rail,OPP,Voltage,ABB,ABB_LDO\r\n")
csv_string = "%s,%s,%d mV,%d,0x%02x\r\n"

# CTRL_CORE_STD_FUSE_OPP_VMIN_IVA_2
decoded_params = decode_fuse_opp_vmin_reg(0x4A0025CC)
if decoded_params[0]: # print values if found, otherwise just skip
    csv.write(csv_string % ("IVA","NOM",decoded_params[1],decoded_params[2],decoded_params[3]))

# CTRL_CORE_STD_FUSE_OPP_VMIN_IVA_3
decoded_params = decode_fuse_opp_vmin_reg(0x4A0025D0)
if decoded_params[0]: # print values if found, otherwise just skip
    csv.write(csv_string % ("IVA","OD",decoded_params[1],decoded_params[2],decoded_params[3]))

# CTRL_CORE_STD_FUSE_OPP_VMIN_IVA_4
decoded_params = decode_fuse_opp_vmin_reg(0x4A0025D4)
if decoded_params[0]: # print values if found, otherwise just skip
    csv.write(csv_string % ("IVA","HIGH",decoded_params[1],decoded_params[2],decoded_params[3]))

# CTRL_CORE_STD_FUSE_OPP_VMIN_IVA_5
if device_type == 574:
    decoded_params = decode_fuse_opp_vmin_reg(0x4A0025C4)
    if decoded_params[0]: # print values if found, otherwise just skip
        csv.write(csv_string % ("IVA","PLUS",decoded_params[1],decoded_params[2],decoded_params[3]))

# CTRL_CORE_STD_FUSE_OPP_VMIN_DSPEVE_2
decoded_params = decode_fuse_opp_vmin_reg(0x4A0025E0)
if decoded_params[0]: # print values if found, otherwise just skip
    csv.write(csv_string % ("DSP","NOM",decoded_params[1],decoded_params[2],decoded_params[3]))

# CTRL_CORE_STD_FUSE_OPP_VMIN_DSPEVE_3
decoded_params = decode_fuse_opp_vmin_reg(0x4A0025E4)
if decoded_params[0]: # print values if found, otherwise just skip
    csv.write(csv_string % ("DSP","OD",decoded_params[1],decoded_params[2],decoded_params[3]))

# CTRL_CORE_STD_FUSE_OPP_VMIN_DSPEVE_4
decoded_params = decode_fuse_opp_vmin_reg(0x4A0025E8)
if decoded_params[0]: # print values if found, otherwise just skip
    csv.write(csv_string % ("DSP","HIGH",decoded_params[1],decoded_params[2],decoded_params[3]))

# CTRL_CORE_STD_FUSE_OPP_VMIN_DSPEVE_5
if device_type == 574:
    decoded_params = decode_fuse_opp_vmin_reg(0x4A0025D8)
    if decoded_params[0]: # print values if found, otherwise just skip
        csv.write(csv_string % ("DSP","PLUS",decoded_params[1],decoded_params[2],decoded_params[3]))

# CTRL_CORE_STD_FUSE_OPP_VMIN_CORE_2
decoded_params = decode_fuse_opp_vmin_reg(0x4A0025F4)
if decoded_params[0]: # print values if found, otherwise just skip
    csv.write(csv_string % ("VDD","NOM",decoded_params[1],decoded_params[2],decoded_params[3]))

# CTRL_CORE_STD_FUSE_OPP_VMIN_GPU_2
decoded_params = decode_fuse_opp_vmin_reg(0x4A003B08)
if decoded_params[0]: # print values if found, otherwise just skip
    csv.write(csv_string % ("GPU","NOM",decoded_params[1],decoded_params[2],decoded_params[3]))

# CTRL_CORE_STD_FUSE_OPP_VMIN_GPU_3
decoded_params = decode_fuse_opp_vmin_reg(0x4A003B0C)
if decoded_params[0]: # print values if found, otherwise just skip
    csv.write(csv_string % ("GPU","OD",decoded_params[1],decoded_params[2],decoded_params[3]))

# CTRL_CORE_STD_FUSE_OPP_VMIN_GPU_4
decoded_params = decode_fuse_opp_vmin_reg(0x4A003B10)
if decoded_params[0]: # print values if found, otherwise just skip
    csv.write(csv_string % ("GPU","HIGH",decoded_params[1],decoded_params[2],decoded_params[3]))

# CTRL_CORE_STD_FUSE_OPP_VMIN_GPU_5
if device_type == 574:
    decoded_params = decode_fuse_opp_vmin_reg(0x4A003B14)
    if decoded_params[0]: # print values if found, otherwise just skip
        csv.write(csv_string % ("GPU","PLUS",decoded_params[1],decoded_params[2],decoded_params[3]))

# CTRL_CORE_STD_FUSE_OPP_VMIN_MPU_1
if device_type == 574:
    decoded_params = decode_fuse_opp_vmin_reg(0x4A003B1C)
    if decoded_params[0]: # print values if found, otherwise just skip
        csv.write(csv_string % ("MPU","LOW",decoded_params[1],decoded_params[2],decoded_params[3]))

# CTRL_CORE_STD_FUSE_OPP_VMIN_MPU_2
decoded_params = decode_fuse_opp_vmin_reg(0x4A003B20)
if decoded_params[0]: # print values if found, otherwise just skip
    csv.write(csv_string % ("MPU","NOM",decoded_params[1],decoded_params[2],decoded_params[3]))

# CTRL_CORE_STD_FUSE_OPP_VMIN_MPU_3
decoded_params = decode_fuse_opp_vmin_reg(0x4A003B24)
if decoded_params[0]: # print values if found, otherwise just skip
    csv.write(csv_string % ("MPU","OD",decoded_params[1],decoded_params[2],decoded_params[3]))

# CTRL_CORE_STD_FUSE_OPP_VMIN_MPU_4
decoded_params = decode_fuse_opp_vmin_reg(0x4A003B28)
if decoded_params[0]: # print values if found, otherwise just skip
    csv.write(csv_string % ("MPU","HIGH",decoded_params[1],decoded_params[2],decoded_params[3]))

# CTRL_CORE_STD_FUSE_OPP_VMIN_MPU_5
if device_type == 574:
    decoded_params = decode_fuse_opp_vmin_reg(0x4A003B2C)
    if decoded_params[0]: # print values if found, otherwise just skip
        csv.write(csv_string % ("MPU","PLUS",decoded_params[1],decoded_params[2],decoded_params[3]))

csv.write("\r\n")

# CTRL_CORE_LDOVBB_IVA_VOLTAGE_CTRL
decoded_params = decode_ldovbb_voltage_ctrl(0x4A002470)
if decoded_params[0]: # print values if found, otherwise just skip
    csv.write("CTRL_CORE_LDOVBB_IVA_VOLTAGE_CTRL,,,,,")
    if decoded_params[1]:
        csv.write("override used,,0x%02x\r\n" % decoded_params[3])
    else:
        csv.write("efuse used,,0x%02x\r\n" % decoded_params[2])

# CTRL_CORE_LDOVBB_DSPEVE_VOLTAGE_CTRL
decoded_params = decode_ldovbb_voltage_ctrl(0x4A00246C)
if decoded_params[0]: # print values if found, otherwise just skip
    csv.write("CTRL_CORE_LDOVBB_DSPEVE_VOLTAGE_CTRL,,,,,")
    if decoded_params[1]:
        csv.write("override used,,0x%02x\r\n" % decoded_params[3])
    else:
        csv.write("efuse used,,0x%02x\r\n" % decoded_params[2])

# CTRL_WKUP_LDOVBB_GPU_VOLTAGE_CTRL
decoded_params = decode_ldovbb_voltage_ctrl(0x4AE0C154)
if decoded_params[0]: # print values if found, otherwise just skip
    csv.write("CTRL_WKUP_LDOVBB_GPU_VOLTAGE_CTRL,,,,,")
    if decoded_params[1]:
        csv.write("override used,,0x%02x\r\n" % decoded_params[3])
    else:
        csv.write("efuse used,,0x%02x\r\n" % decoded_params[2])

# CTRL_WKUP_LDOVBB_MPU_VOLTAGE_CTRL
decoded_params = decode_ldovbb_voltage_ctrl(0x4AE0C158)
if decoded_params[0]: # print values if found, otherwise just skip
    csv.write("CTRL_WKUP_LDOVBB_MPU_VOLTAGE_CTRL,,,,,")
    if decoded_params[1]:
        csv.write("override used,,0x%02x\r\n" % decoded_params[3])
    else:
        csv.write("efuse used,,0x%02x\r\n" % decoded_params[2])

rd1.close()
csv.close()
