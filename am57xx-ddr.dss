/*
 * Copyright (c) 2018, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

debugSessionDAP = ds.openSession("*","CS_DAP_DebugSS");
debugSessionDAP.target.connect();

/* Check ID_CODE register (address 0x4AE0C204) to determine which
   AM57xx variant is being used */
try {
	id_code = debugSessionDAP.memory.readWord(0,0x4AE0C204,false);
} catch(ex) {
	print("\n Trouble reading ID_CODE.\n");
}

print("ID_CODE = 0x" + d2h(id_code));

// Check STD_FUSE_ID_2 register (address 0x4AE0C20C) for package type
try {
	fuse_id_2 = debugSessionDAP.memory.readWord(0,0x4AE0C20C,false);
} catch(ex) {
	print("\n Trouble reading STD_FUSE_ID_2.\n");
}
pkg_type = (fuse_id_2 & 0x00030000) >> 16;  // FUSE_ID_2[17:16] = pkg_type

switch(id_code) {
	case 0x0B9BC02F:
		print("AM571x SR1.0 detected.\n");
		device_type = 571;
		break;
	case 0x1B9BC02F:
		if(pkg_type == 1) {
			print("AM570x SR2.0 detected.\n");
			device_type = 570;
		} else if(pkg_type == 2) {
			print("AM571x SR2.0 detected.\n");
			device_type = 571;
		} else
			print("AM571x/AM570x SR2.0 unrecognized package type\n")
		break;
	case 0x2B9BC02F:
		if(pkg_type == 1) {
			print("AM570x SR2.1 detected.\n");
			device_type = 570;
		} else if(pkg_type == 2) {
			print("AM571x SR2.1 detected.\n");
			device_type = 571;
		} else
			print("AM571x/AM570x SR2.1 unrecognized package type\n")
		break;
	case 0x0B99002F:
		print("AM572x SR1.0 detected.\n");
		device_type = 572;
		break;
	case 0x1B99002F:
		print("AM572x SR1.1 detected.\n");
		device_type = 572;
		break;
	case 0x2B99002F:
		print("AM572x SR2.0 detected.\n");
		device_type = 572;
		break;
	case 0x0BB5002F:
		print("AM574x SR1.0 detected.\n");
		device_type = 574;
		break;
	default:
		print("Unable to identify which AM57xx variant.\n");
		debugSessionDAP.target.disconnect();
		throw("Terminating script.\n")
		break;
}

//Build a filename that includes date/time
var today = new Date();
var year4digit = today.getFullYear();
var month2digit = ("0" + (today.getMonth()+1)).slice(-2);
var day2digit = ("0" + today.getDate()).slice(-2);
var hour2digit = ("0" + today.getHours()).slice(-2);
var minutes2digit = ("0" + today.getMinutes()).slice(-2);
var seconds2digit = ("0" + today.getSeconds()).slice(-2);
var filename_date = '_' + year4digit + '-' + month2digit + '-' + day2digit + '_' + hour2digit + minutes2digit + seconds2digit;
var userHomeFolder = System.getProperty("user.home");
var filename = userHomeFolder + '/Desktop/' + 'am57xx-ddr' + filename_date + '.txt';

file = new java.io.FileWriter(filename);

if (device_type == 570) {
	num_emifs = 1;
} else if (device_type == 571) {
	num_emifs = 1;
} else if (device_type == 572) {
	num_emifs = 2;
} else if (device_type == 574) {
	num_emifs = 2;
} else {
	throw("Error -- code shouldn't get here.")
}

// helper function to create 8-digit hex numbers in ascii format
function d2h(d) {
	// bottom half
	bottom = d & 0xFFFF;
	bottom_ascii = ("0000" + bottom.toString(16)).slice(-4);

	// top half
	top = d >>> 16;  // unsigned right shift - avoids major sign issues...
	top_ascii = ("0000" + top.toString(16)).slice(-4);
	return (top_ascii + bottom_ascii);
}

// helper function to create decimal numbers in ascii format
function d2d(d) {return ((+d).toString());}

var newline = "\n";

file.write("********************** DPLL_DDR **********************" + newline + newline);
// CTRL_CORE_BOOTSTRAP
reg_val = printRegisterValue(debugSessionDAP, "CTRL_CORE_BOOTSTRAP", 0x4A0026C4);
speedselect_pins = (reg_val >> 8) & 3;
switch (speedselect_pins) {
	case 0:
		file.write("  * Reserved value latched by SYSBOOT[9:8]" + newline);
		input_clock = 0;
		break;
	case 1:
		file.write("  * SPEEDSELECT = 20 MHz" + newline);
		input_clock = 20;
		break;
	case 2:
		file.write("  * SPEEDSELECT = 27 MHz" + newline);
		input_clock = 27;
		break;
	case 3:
		file.write("  * SPEEDSELECT = 19.2 MHz" + newline);
		input_clock = 19.2;
		break;
}

// CM_CLKSEL_DPLL_DDR
reg_val = printRegisterValue(debugSessionDAP, "CM_CLKSEL_DPLL_DDR", 0x4A00521C);
dpll_mult = (reg_val >> 8) & 0x7FF;
file.write("  * DPLL_MULT = " + d2d(dpll_mult) + " (x" + d2d(dpll_mult) + ")" + newline);
dpll_div = reg_val & 0x7F;
file.write("  * DPLL_DIV = " + d2d(dpll_div) + " (/" + d2d(dpll_div+1) + ")" + newline);
f_dpll_ddr = input_clock*2*dpll_mult/(dpll_div+1);

// CM_DIV_M2_DPLL_DDR
reg_val = printRegisterValue(debugSessionDAP, "CM_DIV_M2_DPLL_DDR", 0x4A005220);
if (reg_val & (1<<9))  // CLKST = 1
	file.write("  * CLKST = 1: M2 output clock enabled" + newline);
else
	file.write("  * CLKST = 0: M2 output clock disabled" + newline);;
div_m2 = reg_val & 0x1F;
file.write("  * DIVHS = " + d2d(div_m2) + " (/" + d2d(div_m2) + ")" + newline);

// CM_DIV_H11_DPLL_DDR
reg_val = printRegisterValue(debugSessionDAP, "CM_DIV_H11_DPLL_DDR", 0x4A005228);
if (reg_val & (1<<9))  // CLKST = 1
	file.write("  * CLKST = 1: H11 output clock enabled" + newline);
else
	file.write("  * CLKST = 0: H11 output clock disabled" + newline);;
div_h11 = reg_val & 0x1F;
file.write("  * DIVHS = " + d2d(div_h11) + " (/" + d2d(div_h11) + ")" + newline);

file.write(newline + "DPLL_DDR Summary" + newline);
file.write(" -> F_input = " + d2d(input_clock) + " MHz" + newline);
file.write(" -> F_dpll_ddr = " + d2d(f_dpll_ddr) + " MHz" + newline);
file.write(" -> CLKOUT_M2 = EMIF_PHY_GCLK = " + f_dpll_ddr / 2 / div_m2 + " MHz" + newline);
file.write(" -> CLKOUTX2_H11 = EMIF_DLL_GCLK = " + f_dpll_ddr / div_h11 + " MHz" + newline);

file.write(newline);

file.write("********************** DMM - LISA **********************" + newline + newline);
reg_val = printRegisterValue(debugSessionDAP, "DMM_LISA_MAP_0", 0x4E000040);
decodeLisaEntry(reg_val);
reg_val = printRegisterValue(debugSessionDAP, "DMM_LISA_MAP_1", 0x4E000044);
decodeLisaEntry(reg_val);
reg_val = printRegisterValue(debugSessionDAP, "DMM_LISA_MAP_2", 0x4E000048);
decodeLisaEntry(reg_val);
reg_val = printRegisterValue(debugSessionDAP, "DMM_LISA_MAP_3", 0x4E00004C);
decodeLisaEntry(reg_val);
file.write(newline);

file.write("********************** EMIF1 **********************" + newline + newline);

// CTRL_CORE_CONTROL_DDRCACH1_0
reg_val = printRegisterValue(debugSessionDAP, "CTRL_CORE_CONTROL_DDRCACH1_0", 0x4A002E30);
file.write("ddr1_casn, ddr1_rasn, ddr1_rst, ddr1_wen, ddr1_csn[0], ddr1_cke, ddr1_odt[0]" + newline);
InterpretDdrByte(reg_val, 3, 0);
file.write("ddr1_a[15:0]" + newline);
InterpretDdrByte(reg_val, 2, 0);
file.write("ddr1_ba[0], ddr1_ba[1], ddr1_ba[2]" + newline);
InterpretDdrByte(reg_val, 1, 0);
file.write("ddr1_ck, ddr1_nck" + newline);
InterpretDdrByte(reg_val, 0, 1);
file.write(newline);

// CTRL_CORE_CONTROL_DDRCH1_0
reg_val = printRegisterValue(debugSessionDAP, "CTRL_CORE_CONTROL_DDRCH1_0", 0x4A002E38);
file.write("ddr1_d[7:0], ddr1_dqm[0]" + newline);
InterpretDdrByte(reg_val, 3, 0);
file.write("ddr1_dqs[0], ddr1_dqsn[0]" + newline);
InterpretDdrByte(reg_val, 2, 1);
file.write("ddr1_d[15:8], ddr1_dqm[1]" + newline);
InterpretDdrByte(reg_val, 1, 0);
file.write("ddr1_dqs[1], ddr1_dqsn[1]" + newline);
InterpretDdrByte(reg_val, 0, 1);
file.write(newline);

// CTRL_CORE_CONTROL_DDRCH1_1
reg_val = printRegisterValue(debugSessionDAP, "CTRL_CORE_CONTROL_DDRCH1_1", 0x4A002E3C);
file.write("ddr1_d[23:16], ddr1_dqm[2]" + newline);
InterpretDdrByte(reg_val, 3, 0);
file.write("ddr1_dqs[2], ddr1_dqsn[2]" + newline);
InterpretDdrByte(reg_val, 2, 1);
file.write("ddr1_d[31:24], ddr1_dqm[3]" + newline);
InterpretDdrByte(reg_val, 1, 0);
file.write("ddr1_dqs[3], ddr1_dqsn[3]" + newline);
InterpretDdrByte(reg_val, 0, 1);
file.write(newline);

// CTRL_CORE_CONTROL_DDRCH1_2
reg_val = printRegisterValue(debugSessionDAP, "CTRL_CORE_CONTROL_DDRCH1_2", 0x4A002E48);
file.write("ddr1_ecc_d[7:0], ddr1_dqm_ecc" + newline);
InterpretDdrByte(reg_val, 2, 0);
file.write("ddr1_dqs_ecc, ddr1_dqsn_ecc" + newline);
InterpretDdrByte(reg_val, 1, 1);
file.write(newline);

// CTRL_CORE_CONTROL_DDRIO_0
reg_val = printRegisterValue(debugSessionDAP, "CTRL_CORE_CONTROL_DDRIO_0", 0x4A002E50);
file.write("ddr1_d[7:0], ddr1_d[15:8]" + newline);
InterpretDdrVref(reg_val, 15);
file.write("ddr1_d[23:16], ddr1_d[31:24], ddr1_ecc_d[7:0]" + newline);
InterpretDdrVref(reg_val, 10);
file.write(newline);

// If EMIF1 clock is enabled, print the registers
reg_val = debugSessionDAP.memory.readWord(0,0x4A008B30,false);
reg_val = reg_val >> 16;  // Check IDLEST (bits 17:16)
if (reg_val == 3)
	file.write("EMIF1 is disabled." + newline);
else
	printEmifRegs(0x4C000000);

if (num_emifs == 2)
{
	file.write("********************** EMIF2 **********************" + newline + newline);

	// CTRL_CORE_CONTROL_DDRCACH2_0
	reg_val = printRegisterValue(debugSessionDAP, "CTRL_CORE_CONTROL_DDRCACH2_0", 0x4A002E34);
	file.write("ddr2_casn, ddr2_rasn, ddr2_rst, ddr2_wen, ddr2_csn[0], ddr2_cke, ddr2_odt[0]" + newline);
	InterpretDdrByte(reg_val, 3, 0);
	file.write("ddr2_a[15:0]" + newline);
	InterpretDdrByte(reg_val, 2, 0);
	file.write("ddr2_ba[0], ddr2_ba[1], ddr2_ba[2]" + newline);
	InterpretDdrByte(reg_val, 1, 0);
	file.write("ddr2_ck, ddr2_nck" + newline);
	InterpretDdrByte(reg_val, 0, 1);
	file.write(newline);

	// CTRL_CORE_CONTROL_DDRCH2_0
	reg_val = printRegisterValue(debugSessionDAP, "CTRL_CORE_CONTROL_DDRCH2_0", 0x4A002E40);
	file.write("ddr2_d[7:0], ddr2_dqm[0]" + newline);
	InterpretDdrByte(reg_val, 3, 0);
	file.write("ddr2_dqs[0], ddr2_dqsn[0]" + newline);
	InterpretDdrByte(reg_val, 2, 1);
	file.write("ddr2_d[15:8], ddr2_dqm[1]" + newline);
	InterpretDdrByte(reg_val, 1, 0);
	file.write("ddr2_dqs[1], ddr2_dqsn[1]" + newline);
	InterpretDdrByte(reg_val, 0, 1);
	file.write(newline);

	// CTRL_CORE_CONTROL_DDRCH2_1
	reg_val = printRegisterValue(debugSessionDAP, "CTRL_CORE_CONTROL_DDRCH2_1", 0x4A002E44);
	file.write("ddr2_d[23:16], ddr2_dqm[2]" + newline);
	InterpretDdrByte(reg_val, 3, 0);
	file.write("ddr2_dqs[2], ddr2_dqsn[2]" + newline);
	InterpretDdrByte(reg_val, 2, 1);
	file.write("ddr2_d[31:24], ddr2_dqm[3]" + newline);
	InterpretDdrByte(reg_val, 1, 0);
	file.write("ddr2_dqs[3], ddr2_dqsn[3]" + newline);
	InterpretDdrByte(reg_val, 0, 1);
	file.write(newline);

	// CTRL_CORE_CONTROL_DDRIO_1
	reg_val = printRegisterValue(debugSessionDAP, "CTRL_CORE_CONTROL_DDRIO_1", 0x4A002E54);
	file.write("ddr2_d[7:0], ddr2_d[15:8]" + newline);
	InterpretDdrVref(reg_val, 22);
	file.write("ddr2_d[23:16], ddr2_d[31:24]" + newline);
	InterpretDdrVref(reg_val, 17);
	file.write(newline);

	// If EMIF2 clock is enabled, print the registers
	reg_val = debugSessionDAP.memory.readWord(0,0x4A008B38,false);
	reg_val = reg_val >> 16;  // Check IDLEST (bits 17:16)
	if (reg_val == 3)
		file.write("EMIF2 is disabled." + newline);
	else
		printEmifRegs(0x4D000000);

}

print("Data collection complete.");

file.close();
debugSessionDAP.target.disconnect();
print("Created file " + filename);

/* value is the contents read from the register
   byte should be 0-3 where 0 is LSB and 3 is MSB
*/
function InterpretDdrByte(value, byte_num, is_differential)
{
	value = value >> (8*byte_num);
	value = value & 0xFF;

	weak_driver = value & 3;		// Bits 1:0
	slew_rate = (value >> 2) & 7;	// Bits 4:2
	impedance = (value >> 5) & 7;	// Bits 7:5

	if(is_differential)
	{
		switch (weak_driver) {
			case 0:
				file.write("  * Pull logic is disabled" + newline);
				break;
			case 1:
				file.write("  * Pull-up selected for padp, pull-down selected for padn" + newline);
				break;
			case 2:
				file.write("  * Pull-down selected for padp, pull-up selected for padn" + newline);
				break;
			case 3:
				file.write("  * Maintain the previous output value" + newline);
				break;
		}
	} else
	{
		switch (weak_driver) {
			case 0:
				file.write("  * Pull logic is disabled" + newline);
				break;
			case 1:
				file.write("  * Pull-up selected" + newline);
				break;
			case 2:
				file.write("  * Pull-down selected" + newline);
				break;
			case 3:
				file.write("  * Maintain the previous output value" + newline);
				break;
		}
	}
	file.write("  * Slew rate is " + slew_rate + ", where 0=fastest and 7=slowest" + newline);

		switch (impedance) {
		case 0:
			file.write("  * Output Impedance = 80 Ohms" + newline);
			break;
		case 1:
			file.write("  * Output Impedance = 60 Ohms" + newline);
			break;
		case 2:
			file.write("  * Output Impedance = 48 Ohms" + newline);
			break;
		case 3:
			file.write("  * Output Impedance = 40 Ohms" + newline);
			break;
		case 4:
			file.write("  * Output Impedance = 34 Ohms" + newline);
			break;
		default:
			file.write("  * Output Impedance = Reserved" + newline);
			break;
	}
}

function InterpretDdrVref(value, start_bit)
{
	value = value >> start_bit;
	value = value & 0x1F;

	if (value & 1)
	{
		file.write("  * Internal VREF enabled" + newline);
		if (value & 0x10) file.write("  * Capacitor between Vbias and ground" + newline);
		if (value & 0x08) file.write("  * Capacitor between Vbias and vdds_ddr" + newline);
		tap0 = (value >> 2) & 1;
		tap1 = (value >> 1) & 1;
		ddr_tap = (tap1 << 1) | tap0;

		switch(ddr_tap) {
			case 0:
				file.write("  * 2-uA VREF output drive capability" + newline);
				break;
			case 1:
				file.write("  * 4-uA VREF output drive capability" + newline);
				break;
			case 2:
				file.write("  * 8-uA VREF output drive capability" + newline);
				break;
			case 3:
				file.write("  * 32-uA VREF output drive capability" + newline);
				break;
		}
	}
	else
		file.write("  * Internal VREF disabled" + newline);
}

function printEmifRegs(baseaddr)
{
	if (baseaddr == 0x4C000000) {
		reg_val = printRegisterValue(debugSessionDAP, "CTRL_WKUP_EMIF1_SDRAM_CONFIG_EXT", 0x4AE0C144);
		emif1_en_ecc = bits32(reg_val, 16, 16);
		file.write("  * Bit 16: EMIF1_EN_ECC = " + d2d(emif1_en_ecc) + newline);
		reg_val = printRegisterValue(debugSessionDAP, "EMIF_ECC_CTRL_REG", 0x4C000110);
		reg_ecc_en = bits32(reg_val, 31, 31);
		file.write("  * Bit 31: reg_ecc_en = " + d2d(reg_ecc_en) + newline);
		reg_ecc_addr_rgn_prot = bits32(reg_val, 30, 30);
		if (reg_ecc_addr_rgn_prot==0) {
			insideoutside = "outside";
		} else {
			insideoutside = "inside";
		}
		file.write("  * Bit 30: reg_ecc_addr_rgn_prot = " + d2d(reg_ecc_addr_rgn_prot) + newline);
		reg_ecc_verify_dis = bits32(reg_val, 29, 29);
		if (reg_ecc_verify_dis==0) {
			file.write("  * Bit 29: reg_ecc_verify_dis = 0, enable ECC verification on reads (normal)" + newline);
		} else {
			file.write("  * Bit 29: reg_ecc_verify_dis = 1, disable ECC verification on reads (WARNING: NOT EXPECTED)" + newline);
		}
		reg_rmw_en = bits32(reg_val, 28, 28);
		if (reg_rmw_en == 1) {
			if (device_type != 574) {
				file.write("  * Bit 28: ERROR, reg_rmw_en only exists on AM574x family" + newline);
			}
		}
		if (reg_ecc_en==1 && reg_rmw_en==0 && device_type==574) {
			file.write("  * Bit 28: WARNING, reg_rmw_en is expected to be enabled when using ECC" + newline);
		}
		if (reg_ecc_en==1 && reg_rmw_en==1 && device_type==574) {
			file.write("  * Bit 28: reg_rmw_en=1,  this is the recommended configuration when using ECC" + newline);
		}
		reg_ecc_addr_rgn_2_en = bits32(reg_val, 1, 1);
		file.write("  * Bit 1: reg_ecc_addr_rgn_2_en = " + d2d(reg_ecc_addr_rgn_2_en) + newline);
		reg_ecc_addr_rgn_1_en = bits32(reg_val, 0, 0);
		file.write("  * Bit 0: reg_ecc_addr_rgn_1_en = " + d2d(reg_ecc_addr_rgn_1_en) + newline);
		reg_val = printRegisterValue(debugSessionDAP, "EMIF_ECC_ADDRESS_RANGE_1", 0x4C000114);
		reg_ecc_end_addr_1 = bits32(reg_val, 31, 16);
		reg_ecc_strt_addr_1 = bits32(reg_val, 15, 0);
		reg_val = printRegisterValue(debugSessionDAP, "EMIF_ECC_ADDRESS_RANGE_2", 0x4C000118);
		reg_ecc_end_addr_2 = bits32(reg_val, 31, 16);
		reg_ecc_strt_addr_2 = bits32(reg_val, 15, 0);
		if (reg_ecc_en==1) {
			if (reg_ecc_addr_rgn_1_en==1) {
				file.write("  -> ECC Region 1 is enabled to protect " + insideoutside + " the address range 0x");
				file.write(d2h((reg_ecc_strt_addr_1<<16) + 0x80000000) + " to 0x");
				file.write(d2h((reg_ecc_end_addr_1<<16) + 0x8000FFFF) + newline);
			} else {
				file.write("  -> ECC Region 1 is disabled." + newline);
			}
			if (reg_ecc_addr_rgn_2_en==1) {
				file.write("  -> ECC Region 2 is enabled to protect " + insideoutside + " the address range 0x");
				file.write(d2h((reg_ecc_strt_addr_2<<16) + 0x80000000) + " to 0x");
				file.write(d2h((reg_ecc_end_addr_2<<16) + 0x8000FFFF) + newline);
			} else {
				file.write("  -> ECC Region 2 is disabled." + newline);
			}
		}
		printRegisterValue(debugSessionDAP, "EMIF_1B_ECC_ERR_CNT", 0x4C000130);
		printRegisterValue(debugSessionDAP, "EMIF_1B_ECC_ERR_THRSH", 0x4C000134);
		printRegisterValue(debugSessionDAP, "EMIF_1B_ECC_ERR_DIST_1", 0x4C000138);
		printRegisterValue(debugSessionDAP, "EMIF_1B_ECC_ERR_DIST_1", 0x4C00013C);
		printRegisterValue(debugSessionDAP, "EMIF_2B_ECC_ERR_ADDR_LOG", 0x4C000140);
	} else
		reg_val = printRegisterValue(debugSessionDAP, "CTRL_WKUP_EMIF2_SDRAM_CONFIG_EXT", 0x4AE0C148);

	reg_val = printRegisterValue(debugSessionDAP, "EMIF_STATUS", baseaddr + 0x04);
	file.write(newline);

	reg_val = printRegisterValue(debugSessionDAP, "EMIF_SDRAM_CONFIG", baseaddr + 0x08);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_SDRAM_CONFIG_2", baseaddr + 0x0C);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_SDRAM_REFRESH_CONTROL", baseaddr + 0x10);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_SDRAM_TIMING_1", baseaddr + 0x18);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_SDRAM_TIMING_2", baseaddr + 0x20);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_SDRAM_TIMING_3", baseaddr + 0x28);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_READ_WRITE_LEVELING_RAMP_WINDOW", baseaddr + 0xD4);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_READ_WRITE_LEVELING_RAMP_CONTROL", baseaddr + 0xD8);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_READ_WRITE_LEVELING_CONTROL", baseaddr + 0xDC);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_DDR_PHY_CONTROL_1", baseaddr + 0xE4);
	file.write("  * Bits 4:0 READ_LATENCY = " + d2d(reg_val & 0x1F) + newline);
	file.write("  * Bit 9 PHY_FAST_DLL_LOCK = " + d2d((reg_val >> 9)&1) + newline);
	file.write("  * Bits 17:10 PHY_DLL_LOCK_DIFF = " + d2d((reg_val >> 10)&0xFF) + newline);
	file.write("  * Bit 18 PHY_INVERT_CLKOUT = " + d2d((reg_val >> 18)&1) + newline);
	file.write("  * Bit 19 PHY_DIS_CALIB_RST = " + d2d((reg_val >> 19)&1) + newline);
	file.write("  * Bit 20 PHY_CLK_STALL_LEVEL = " + d2d((reg_val >> 20)&1) + newline);
	file.write("  * Bit 21 PHY_HALF_DELAYS = " + d2d((reg_val >> 21)&1) + newline);
	file.write("  * Bit 25 WRLVL_MASK = " + d2d((reg_val >> 25)&1) + newline);
	file.write("  * Bit 26 RDLVLGATE_MASK = " + d2d((reg_val >> 26)&1) + newline);
	file.write("  * Bit 27 RDLVL_MASK = " + d2d((reg_val >> 27)&1) + newline);
	file.write(newline);

	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_1 ", baseaddr + 0x200);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_2 ", baseaddr + 0x208);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_3 ", baseaddr + 0x210);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_4 ", baseaddr + 0x218);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_5 ", baseaddr + 0x220);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_6 ", baseaddr + 0x228);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_7 ", baseaddr + 0x230);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_8 ", baseaddr + 0x238);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_9 ", baseaddr + 0x240);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_10", baseaddr + 0x248);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_11", baseaddr + 0x250);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_12", baseaddr + 0x258);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_13", baseaddr + 0x260);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_14", baseaddr + 0x268);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_15", baseaddr + 0x270);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_16", baseaddr + 0x278);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_17", baseaddr + 0x280);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_18", baseaddr + 0x288);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_19", baseaddr + 0x290);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_20", baseaddr + 0x298);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_21", baseaddr + 0x2A0);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_22", baseaddr + 0x2A8);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_23", baseaddr + 0x2B0);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_24", baseaddr + 0x2B8);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_25", baseaddr + 0x2C0);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_26", baseaddr + 0x2C8);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_27", baseaddr + 0x2D0);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_28", baseaddr + 0x2D8);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_29", baseaddr + 0x2E0);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_30", baseaddr + 0x2E8);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_31", baseaddr + 0x2F0);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_32", baseaddr + 0x2F8);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_33", baseaddr + 0x300);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_34", baseaddr + 0x308);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_35", baseaddr + 0x310);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_CONTROL_36", baseaddr + 0x318);
	file.write(newline);

	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_1 ", baseaddr + 0x144);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_2 ", baseaddr + 0x148);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_3 ", baseaddr + 0x14C);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_4 ", baseaddr + 0x150);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_5 ", baseaddr + 0x154);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_6 ", baseaddr + 0x158);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_7 ", baseaddr + 0x15C);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_8 ", baseaddr + 0x160);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_9 ", baseaddr + 0x164);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_10", baseaddr + 0x168);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_11", baseaddr + 0x16C);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_12", baseaddr + 0x170);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_13", baseaddr + 0x174);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_14", baseaddr + 0x178);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_15", baseaddr + 0x17C);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_16", baseaddr + 0x180);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_17", baseaddr + 0x184);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_18", baseaddr + 0x188);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_19", baseaddr + 0x19C);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_20", baseaddr + 0x190);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_21", baseaddr + 0x194);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_22", baseaddr + 0x198);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_23", baseaddr + 0x19C);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_24", baseaddr + 0x1A0);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_25", baseaddr + 0x1A4);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_26", baseaddr + 0x1A8);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_27", baseaddr + 0x1AC);
	reg_val = printRegisterValue(debugSessionDAP, "EMIF_EXT_PHY_STATUS_28", baseaddr + 0x1B0);
	file.write(newline);
}

function printRegisterValue(ds, name, addr)
{
	value = ds.memory.readWord(0,addr,false);
	value_string = d2h(value);
	file.write(name + " = 0x" + value_string + newline);
	return value; // return the register value for interrogation
}

function decodeLisaEntry(lisa_val)
{
	// Don't print anything for unmapped entries
	sdrc_map = (lisa_val >> 8) & 3;
	if ( sdrc_map == 0 )
		return;

	file.write("  * System Address Mapping = 0x" + d2h(lisa_val & 0xFF000000) + newline);
	val = (lisa_val >> 20) & 7;
	sys_size = Math.pow(2,val)*16;
	file.write("  * Section Size = " + d2d(sys_size) + " MB" + newline);

	sdrc_intl = (lisa_val >> 18) & 3;
	if ( sdrc_map == 3 )
	{
		file.write("  * Mapped to EMIF1 and EMIF2: ");
		if ( sdrc_intl == 0 ) file.write("Illegal combo of SDRC_INTL and SDRC_MAP");
		if ( sdrc_intl == 1 ) file.write("128-byte interleaving");
		if ( sdrc_intl == 2 ) file.write("256-byte interleaving");
		if ( sdrc_intl == 3 ) file.write("512-byte interleaving");
		file.write(newline);
	}
	else
		file.write("  * Mapped to EMIF" + d2d(sdrc_map) + newline);
}

// Inputs:
//   Data - 32-bit register value
//   Upper - Highest bit to keep
//   Lower - Lowest bit to keep
//   (bit 0 refers to LSB, bit 31 to MSB)
// Return: right aligned data
function bits32(data, upper, lower)
{
	data = data >>> lower; // unsigned right-shift
	upper = upper - lower;
	bitmask =  0xFFFFFFFF >>> (31 - upper);
	return (data & bitmask);
}
